# TDT4250 - Webpage Transformation 
## Name: Simon SmeetsI worked on this Coursework task with **Tyler McAllister**.
The aim of this coursework is to generate .html pages from the predefined .xmi model that was created from the previous coursework task. Our transformation is M2T.

## Requirements and Run Instructions
To run the webpage generator you will need the Eclipse EMF Ecore XMI .jar located here:
**https://mvnrepository.com/artifact/org.eclipse.emf/org.eclipse.emf.ecore.xmi/2.15.0**

The **CourseworkGen.java** file must be run to generate the webpages. The argument to be passed to its main method must be the file path location of the **NTNU.xmi** file which can be found in the project's **model** folder.
After being run the .html files will be created in the **model** folder. You may have to refresh the folder in Eclipse to view them. The webpages can be viewed in Eclipse itself or in a web browser.
Courses TDT4250(**ntnuwebpage0.html**) and TDT4100(**ntnuwebpage1.html**) are the main files to be inspected as these courses contain the most information in the NTNU.xmi file, but every course also has its own generated webpage.

## Important Files/Directories
- **src**: Contains the classes that generate the webpages.
- **src-gen**: Contains model code, implementation and util packages
- **model**: Contains ecore, genmodel and NTNU.xmi which has the TDT4250 course and TDT4100 course details. This is also where the .html pages will be generated.

## Implementation Details
- **CourseworkGen.java**: Located in the src folder. This class takes the .xmi as an argument and uses the **CourseworkResourceFactoryImpl** to return a University eObject.
- **CourseworkHTMLGenerator.java**: Located in the src folder. Takes the University eObject and iterates through each course within the university to generate an ArrayList of webpages. This ArrayList is then iterated through and each course is saved to its own .html file.